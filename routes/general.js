import express from 'express';

import { getDashboardStats, getUserById } from '../controller/general.js';

const router = express.Router();

router.get('/users/:id', getUserById);
router.get('/dashboard', getDashboardStats);

export default router;
